import { Injectable, ViewContainerRef } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
 
@Injectable()
export class AlertService {
 
    constructor(
        public toastr: ToastsManager,
    ) { }

    setRootViewContainerReft(vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr)
    }
 
    success(message: string) {
        this.toastr.success(message, 'Success!')
    }
 
    error(message: string) {
        this.toastr.error(message, 'Oops!')
    }

    warning(message: string) {
        this.toastr.warning(message, 'Alert!')
    }

    info(message: string) {
        this.toastr.info(message)
    }

    showStudentPassword(message: string) {
        this.toastr.info(message, 'Your password is', {toastLife: 10000})
    }
}