import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { AlertService } from './alert.service'
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';

 
@Injectable()
export class AuthenticationService {

    authState: any

    constructor(
        public afAuth: AngularFireAuth, 
        public af: AngularFireDatabase,
        private router: Router,
        private alertService: AlertService) { 
            this.authState = JSON.parse(localStorage.getItem("currentUser"))
        }
 
    isAuthenticated() {
        return this.authState        
    }

    teacherLogin(email: string, password: string) {
        this.afAuth.auth.signInWithEmailAndPassword(email, password)
            .then((success)=>{
                this.authState = {
                    type: 'teacher',
                    email: email
                }
                localStorage.setItem('currentUser', JSON.stringify(this.authState))
                this.router.navigate(['/admin/dashboard'])
            }, (error)=>{
                this.alertService.error(error.message)
            })        
    }

    studentLogin(ID: string, password: string) {
        this.af.list('/students', {
            query: {
                orderByChild: 'ID',
                equalTo: ID
            }
        }).subscribe((data)=>{
            if(data.length == 0){
                this.alertService.error("Wrong ID or password")
            } else {
                this.authState = {
                    type: 'student',
                    ID: ID
                }
                localStorage.setItem('currentUser', JSON.stringify(this.authState))
                this.router.navigate(['/user/challenge-select'])
            }
        })        
    }

    teacherSignUp(email: string, password: string) {
        this.afAuth.auth.createUserWithEmailAndPassword(email, password).then( (success) => {
            this.alertService.success("Registered successfully. Please Sing In.",)
        }, (error) => {
            this.alertService.error(error.message)
        })
    }

    teacherForgotPassword(email: string) {
        this.afAuth.auth.sendPasswordResetEmail(email).then(success => {
            this.alertService.success("Password reset email sent.")
        }, error => {
            this.alertService.error(error.message)
        })
    }

    checkStudent(ID: string) {
        return new Promise((resolve, reject) => {
            this.af.list('students', {
                query: {
                    orderByChild: 'ID',
                    equalTo: ID
                }
            }).subscribe((users) => {
                if (users.length > 0) {
                    reject(users);
                } else {
                    resolve();
                }
            });
        });
    }

    studentSignUp(ID: string, password: string, security: string) {
        this.checkStudent(ID).then(res => {
            this.af.list('students').push({ID, password, security}).then( (success) => {
                this.alertService.success("Registered successfully. Please Sing In.")
            }, (error) => {
                this.alertService.error(error.message)
            })
        }, error => {
            this.alertService.error("That ID has already used.");
        });
    }

    studentForgotPassword(ID: string, security: string) {
        console.log(ID, security)
        this.checkStudent(ID).then(res => {
            this.alertService.error("That ID is not exist.")
        }, error => {
            if(error[0].security == security) {
                this.alertService.showStudentPassword(error[0].password)
            } else {
                this.alertService.error("Wrong security answer.")
            }
        });
    }
 
    logout() {
        // remove user from local storage to log user out
        if(this.authState.type == 'teacher')
        this.authState = null
        localStorage.removeItem('currentUser');
        this.router.navigate(['/'])
    }
}