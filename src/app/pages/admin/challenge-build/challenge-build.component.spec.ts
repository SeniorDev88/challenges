import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChallengeBuildComponent } from './challenge-build.component';

describe('ChallengeBuildComponent', () => {
  let component: ChallengeBuildComponent;
  let fixture: ComponentFixture<ChallengeBuildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChallengeBuildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengeBuildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
