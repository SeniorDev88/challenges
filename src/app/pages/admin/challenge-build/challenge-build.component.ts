import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { AlertService } from '../../../services/index'

declare let $: any;

@Component({
  selector: 'app-challenge-build',
  templateUrl: './challenge-build.component.html',
  styleUrls: ['./challenge-build.component.css'],
  providers: [AlertService]
})

export class ChallengeBuildComponent {

  typeEdit: boolean = false
  id: string = ''
  challenge: FirebaseObjectObservable<any>
  title: string = 'Challenge Title'
  description: string = 'This challenge is about.. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis,'
  sliders: any[] = []
  items: FirebaseListObservable<any[]>
  totalChange: number
  

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public af: AngularFireDatabase,
    private alertService: AlertService,
  ) {
    this.route
      .queryParams
      .subscribe(params => {
        console.log(params['id'])
        if(params['id']) {
          this.id = params['id']
          this.typeEdit = true
          this.challenge = this.af.object(`/challenges/${this.id}`)
          this.challenge.subscribe( (data) => {
            this.title = data.title
            this.description = data.description
            this.sliders = data.sliders
          })
        } else {
          this.addSlider()
          this.items = this.af.list('/challenges')
        }
      })
  }

    Send() {
        if(this.typeEdit) {
            this.af.object(`/challenges/${this.id}`).update({
                title: this.title,
                description: this.description,
                sliders: this.sliders,
            })
        } else {
            let avgAnswers: number[] = []
            for( let index = 0 ; index < this.sliders.length ; index++ ) {
                avgAnswers.push(0)
            }
            this.items.push({
                type: 'slider-challenge',
                title: this.title,
                description: this.description,
                sliders: this.sliders,
                avgAnswers: avgAnswers
            }).then( (success) => {
                this.alertService.success("Challenge added successfully.")
            }, (error) => {
                this.alertService.error(error.message)
            })
        }    
    }

    onUpdate(event, cSlider) {
    }

    addSlider() {
        this.sliders.push({
            id: this.sliders.length,
            name: 'answer',
            value: 0,
            description: 'Question'
        })
        const value = Math.floor(100/this.sliders.length)
        for(let index = 0 ; index < this.sliders.length ; index++) {
            this.sliders[index].value = value
        }
        this.sliders[0].value += 100%this.sliders.length
    }

    removeSlider() {
        this.sliders.pop()
        const value = Math.floor(100/this.sliders.length)
        for(let index = 0 ; index < this.sliders.length ; index++) {
            this.sliders[index].value = value
        }
        this.sliders[0].value += 100%this.sliders.length
    }

}
