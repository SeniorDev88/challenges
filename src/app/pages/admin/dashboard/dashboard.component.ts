import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  challenges: FirebaseListObservable<any[]>
  constructor(
    public af: AngularFireDatabase
  ) { 
    this.challenges = this.af.list('/challenges')
  }

  ngOnInit() {
  }

}
