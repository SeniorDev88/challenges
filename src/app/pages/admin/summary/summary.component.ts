import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {

  challenges: FirebaseListObservable<any[]>

  constructor(public af: AngularFireDatabase) {
    this.challenges = this.af.list('/challenges')
  }

  ngOnInit() {
  }

}
