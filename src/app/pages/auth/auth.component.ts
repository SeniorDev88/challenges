import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
 
import { AlertService, AuthenticationService } from '../../services/index';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
  providers: [AlertService, AuthenticationService]
})
export class AuthComponent implements OnInit {

    path: string
    model: any = {}

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService) { }
 
    ngOnInit() {
        let authState = this.authenticationService.isAuthenticated()
        if(authState) {
            if(authState.type == 'teacher') {
                this.router.navigate(['/admin/dashboard'])
            }
            if(authState.type == 'student') {
                this.router.navigate(['/user/challenge-select'])
            }
        }
        this.path = this.router.url
    }
 
    submit() {
        switch(this.path) {
            case '/sign-in':
                if(this.model.authType == 'Teacher') {
                    this.authenticationService.teacherLogin(this.model.username, this.model.password)
                } else {
                    this.authenticationService.studentLogin(this.model.username, this.model.password)
                }
                break
            case '/sign-up':
                if(this.model.authType == 'Teacher') {
                    this.authenticationService.teacherSignUp(this.model.username, this.model.password)
                } else {
                    this.authenticationService.studentSignUp(this.model.username, this.model.password, this.model.security)
                }
                break
            default:
                if(this.model.authType == 'Teacher') {
                    this.authenticationService.teacherForgotPassword(this.model.username)
                } else {
                    this.authenticationService.studentForgotPassword(this.model.username, this.model.security)
                }
                break
        }  
    }
}
