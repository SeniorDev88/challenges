import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Component({
  selector: 'app-challenge-select',
  templateUrl: './challenge-select.component.html',
  styleUrls: ['./challenge-select.component.css']
})
export class ChallengeSelectComponent implements OnInit {

  challenges: FirebaseListObservable<any[]>
  constructor(
    public af: AngularFireDatabase
  ) { 
    this.challenges = this.af.list('/challenges')
  }

  ngOnInit() {
  }

}
