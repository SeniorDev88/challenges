import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import { ResourceService } from '../../../services/index'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [ResourceService]
})
export class ProfileComponent implements OnInit {

  model: any = {}
  countryList: string[]
  countryControl: FormControl
  filteredStates: Observable<any[]>;

  constructor(
    private resourceService: ResourceService
  ) {
    this.countryList = this.resourceService.getCountryList()
    this.countryControl = new FormControl();
    this.filteredStates = this.countryControl.valueChanges
        .startWith(null)
        .map(state => state ? this.filterStates(state) : this.countryList.slice());
  }

  ngOnInit() {
  }

  filterStates(name: string) {
    return this.countryList.filter(state =>
      state.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  saveProfile() {
    this.model.country = this.countryControl.value
    console.log(this.model)
  }
}
