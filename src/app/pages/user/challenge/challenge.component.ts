import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { AlertService } from '../../../services/index';

@Component({
  selector: 'app-challenge',
  templateUrl: './challenge.component.html',
  styleUrls: ['./challenge.component.css'],
  providers: [AlertService]
})
export class ChallengeComponent implements OnInit {

  id: string
  challenge: FirebaseObjectObservable<any>
  data: any
  loaded: boolean = false

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private af: AngularFireDatabase,
    private alertService: AlertService,
  ) { 

    this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.id = params['id']
      })
    this.challenge = this.af.object(`/challenges/${this.id}`)
    this.challenge.subscribe( (data) => {
      this.data = data
      this.loaded = true
    })
  }

  ngOnInit() {
  }

}
