import { Component, ViewContainerRef } from '@angular/core';
import { AlertService } from './services/index';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AlertService]
})
export class AppComponent {
  constructor(
    vcr: ViewContainerRef,
    private alertService: AlertService
  ) { 
    this.alertService.setRootViewContainerReft(vcr)
  }
}
