import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SliderChallengeComponent } from './slider-challenge.component';

describe('SliderChallengeComponent', () => {
  let component: SliderChallengeComponent;
  let fixture: ComponentFixture<SliderChallengeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SliderChallengeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderChallengeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
