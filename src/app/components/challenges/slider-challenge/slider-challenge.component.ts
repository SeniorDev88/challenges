import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AlertService } from '../../../services/index'

declare let $: any;

@Component({
  selector: 'app-slider-challenge',
  templateUrl: './slider-challenge.component.html',
  styleUrls: ['./slider-challenge.component.css']
})
export class SliderChallengeComponent implements OnInit {

  @Input() title: string
  @Input() description: string
  @Input() sliders: any[] = []
  @Input() ID: string

  local: any[] = []
  names: string[] = []
  localValues: number[] = []
  results: FirebaseListObservable<any[]>
  totalChange: number
  submitted: boolean = false
  
  chartOptions = {
    tooltips: {
      callbacks: {
        label: function(tooltipItem, data) {
					var datasetLabel = "";
					var label = data.labels[tooltipItem.index];
					return label + ' : ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + '%';
				}
      }
    }
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public af: AngularFireDatabase,
    private alertService: AlertService,
  ) { }

  ngOnInit() {      
    for(let slider of this.sliders) {
      this.names.push(slider.name)
      this.localValues.push(slider.value)
    }
    this.results = this.af.list('/results',{
        query: {
          orderByChild: 'StudentID',
          equalTo: JSON.parse(localStorage.getItem('currentUser')).ID
        }
    })
    this.results.subscribe(data => {
      let answer = data.find( item => item.ID === this.ID )
      if(answer) {
        this.submitted = true        
        for(let index = 0 ; index < this.sliders.length ; index ++) {
          this.local[index] = {
            value: answer.values[index],
            name: this.sliders[index].name
          }
        }
      } else {
        this.local = this.sliders
        this.results = this.af.list('/results',{
            query: {
              orderByChild: 'ID',
              equalTo: this.ID
            }
        })
      }
      this.localValues = []
      for(let slider of this.local) {
        this.localValues.push(slider.value)
      }
    })

  }

  updateLocalValues(cSlider) {
    if( Math.abs(this.totalChange) <   this.local.length ) {
      return
    }

    let numZero = 0
    let offset = 0
      this.local.forEach(element => {
      if( element.value == 0)
        numZero++
    })
    if(cSlider.value == 0) {
      numZero--
    }
    if(numZero ==   this.local.length - 1 ) {
      numZero = 0
    }
    if(this.totalChange>0) {
      offset = Math.floor(this.totalChange / (  this.local.length - 1))
    } else {
      offset = Math.floor(this.totalChange / (  this.local.length - 1 - numZero))
    }
    let unavailable = this.totalChange - offset * (  this.local.length - 1 - numZero)
    for( let i = 0 ; i <   this.local.length ; i++ ) {
      if(   this.local[i].id != cSlider.id && (   this.local[i].value != 0 || offset > 0 ) ) {
          this.local[i].value += offset
        if(  this.local[i].value < 0) {
          unavailable +=   this.local[i].value
            this.local[i].value = 0
        }
      }
    }
    this.totalChange = unavailable
    this.updateLocalValues(cSlider)
  }

  Send() {
    if(this.submitted) {
      this.alertService.error("You already submitted your answer!")
      return
    }
    this.results.push({
       ID: this.ID,
       StudentID: JSON.parse(localStorage.getItem('currentUser')).ID,
       values: this.localValues
    }).then( (success) => {
        this.alertService.success("Your answer have been successfully submitted.")
        this.router.navigate(['/user/challenge-select'])
    }, (error) => {
        this.alertService.error(error.message)
    })
  }

  onUpdate(event, cSlider) {
    for( let i = 0 ; i<  this.local.length ; i++ ) {
        this.local[i].value = this.localValues[i]
    }
  }

  OnFinish(event, cSlider) {
    this.totalChange = cSlider.value - event.from
    this.updateLocalValues(cSlider)
    this.localValues = []
    for( let slider of   this.local ) {
      if( slider.id == cSlider.id ) {
        slider.value = event.from + this.totalChange
      }
      this.localValues.push(slider.value)
    }
  }

  updateItems(event) {
    this.results = this.af.list('/results',{
        query: {
          orderByChild: 'ID',
          equalTo: this.ID
        }
    })
  }

}
