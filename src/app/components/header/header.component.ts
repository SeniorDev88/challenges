import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService, AuthenticationService } from '../../services/index';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [AlertService, AuthenticationService]
})
export class HeaderComponent implements OnInit {

  constructor(
    public authenticationService: AuthenticationService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  isOnPage(path: string): boolean {
    return this.router.url.includes(path)
  }

  signOut() {
    this.authenticationService.logout()
  }

}
