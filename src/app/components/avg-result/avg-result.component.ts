import { Component, OnInit, Input } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

@Component({
  selector: 'app-avg-result',
  templateUrl: './avg-result.component.html',
  styleUrls: ['./avg-result.component.css']
})
export class AvgResultComponent implements OnInit {

  @Input() ID: string
  results: FirebaseListObservable<any[]>
  challenge: FirebaseObjectObservable<any>
  answers: string[] = []
  values: number[] = []

  chartOptions = {
    tooltips: {
      callbacks: {
        label: function(tooltipItem, data) {
					var datasetLabel = "";
					var label = data.labels[tooltipItem.index];
					return label + ' : ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + '%';
				}
      }
    }
  }
  constructor(public af: AngularFireDatabase) {
  }

  ngOnInit() {
    let temp: number[] = []
    this.results = this.af.list('/results',{
        query: {
          orderByChild: 'ID',
          equalTo: this.ID
        }
    })
    this.challenge = this.af.object(`/challenges/${this.ID}`)
    this.challenge.subscribe( data => {
      for( let slider of data.sliders ){
        this.answers.push(slider.name)
        this.values.push(1)
        temp.push(0)
      }    
      this.results.subscribe( data => {
        data.map((result) => {
          result.values.map( (value, index) => {
            temp[index] += value
          })
        })
        temp.forEach( (value, index) => {
          temp[index] /= data.length
        })
        this.values = temp
      })
    })
  }
}
