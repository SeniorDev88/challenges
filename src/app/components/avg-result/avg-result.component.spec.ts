import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvgResultComponent } from './avg-result.component';

describe('AvgResultComponent', () => {
  let component: AvgResultComponent;
  let fixture: ComponentFixture<AvgResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvgResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvgResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
