import { Routes } from '@angular/router';

//Auth Pages
import { AuthComponent } from './pages/auth/auth.component';
//Admin Pages
import { AdminComponent } from './pages/admin/admin.component';
import { DashboardComponent } from './pages/admin/dashboard/dashboard.component';
import { ChallengeBuildComponent } from './pages/admin/challenge-build/challenge-build.component';
import { SummaryComponent } from './pages/admin/summary/summary.component';
//User Pages
import { UserComponent } from './pages/user/user.component';
import { ChallengeComponent } from './pages/user/challenge/challenge.component';
import { ChallengeSelectComponent } from './pages/user/challenge-select/challenge-select.component';
import { ProfileComponent } from './pages/user/profile/profile.component';
//Error pages
import { NoContentComponent } from './pages/no-content/no-content.component';



export const ROUTES: Routes = [
  
    { path: '', redirectTo:'sign-in',   pathMatch: 'full'},
    { path: 'sign-in',                  component: AuthComponent },
    { path: 'sign-up',                  component: AuthComponent },
    { path: 'forgot',                   component: AuthComponent },
    { path: 'admin',      component: AdminComponent, children: [
        { path: 'dashboard',            component: DashboardComponent },
        { path: 'create-challenge',     component: ChallengeBuildComponent},
        { path: 'edit-challenge',       component: ChallengeBuildComponent},
        { path: 'summary',              component: SummaryComponent }
    ]},
    { path: 'user',       component: UserComponent, children: [
        { path: 'challenge-select',     component: ChallengeSelectComponent },
        { path: 'challenge',            component: ChallengeComponent },
        { path: 'profile',     component: ProfileComponent }
    ]},
    { path: '**',      component: NoContentComponent},
];
