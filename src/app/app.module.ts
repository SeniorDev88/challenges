import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { MdSelectModule, MdRadioModule, MdCardModule, MdInputModule, MdAutocompleteModule } from '@angular/material';


//Ng-Components
import { IonRangeSliderModule } from "ng2-ion-range-slider";
import { ChartsModule } from 'ng2-charts';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { InlineEditorModule } from 'ng2-inline-editor';
import { Ng4GeoautocompleteModule } from 'ng4-geoautocomplete';

// ---------------App---------------
import { AppComponent } from './app.component';
import { ROUTES } from './app.routes';
import { NoContentComponent } from './pages/no-content/no-content.component';
//Components
import { HeaderComponent } from './components/header/header.component';
import { SliderChallengeComponent } from './components/challenges/slider-challenge/slider-challenge.component';
//Auth Pages
import { AuthComponent } from './pages/auth/auth.component';
//Admin Pages
import { AdminComponent } from './pages/admin/admin.component';
import { DashboardComponent } from './pages/admin/dashboard/dashboard.component';
import { SummaryComponent } from './pages/admin/summary/summary.component';
import { ChallengeBuildComponent } from './pages/admin/challenge-build/challenge-build.component';
//User Pages
import { UserComponent } from './pages/user/user.component';
import { ChallengeComponent } from './pages/user/challenge/challenge.component';
import { ChallengeSelectComponent } from './pages/user/challenge-select/challenge-select.component';
import { ProfileComponent } from './pages/user/profile/profile.component';
//Firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AvgResultComponent } from './components/avg-result/avg-result.component';




export const firebaseConfig = {
    apiKey: "AIzaSyC6K8uuJv7qn32fJPFIa3KfniKWUMkvfW4",
    authDomain: "slidermaster-44646.firebaseapp.com",
    databaseURL: "https://slidermaster-44646.firebaseio.com",
    projectId: "slidermaster-44646",
    storageBucket: "slidermaster-44646.appspot.com",
    messagingSenderId: "497486098855"
};

@NgModule({
  declarations: [

    //App
    AppComponent,
    NoContentComponent,  

    //Common
    HeaderComponent,
    SliderChallengeComponent,
    AvgResultComponent,

    //Auth pages
    AuthComponent,

    //Admin pages
    AdminComponent,
    DashboardComponent,
    SummaryComponent,
    ChallengeBuildComponent,
    
    //User pages
    UserComponent,
    ChallengeComponent,
    ChallengeSelectComponent,
    ProfileComponent, 
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ChartsModule,
    IonRangeSliderModule,
    InlineEditorModule,

    //Material
    MdSelectModule,
    MdRadioModule,
    MdCardModule,
    MdInputModule,
    MdAutocompleteModule,

    //Firebase
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,

    RouterModule.forRoot(ROUTES),
    ToastModule.forRoot(),
    Ng4GeoautocompleteModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
